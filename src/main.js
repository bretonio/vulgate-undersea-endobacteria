import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import MuseUI from 'muse-ui';
import 'muse-ui/dist/muse-ui.css'

Vue.use(VueRouter)
Vue.use(MuseUI)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
